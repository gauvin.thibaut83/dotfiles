ANSIBLE_VENV=venv/bin/activate
PYTHON_VERSION=3.10

# https://pypi.org/project/ansible/#history
ANSIBLE_VERSION="10.4.0"
# https://pypi.org/project/ansible-lint/#history
ANSIBLE_LINT_VERSION="24.9.2"
# https://pypi.org/project/yamllint/#history
YAMLLINT_VERSION="1.35.1"
# https://pypi.org/project/molecule/#history
MOLECULE_VERSION="24.9.0"
# https://pypi.org/project/molecule-plugins/#history
MOLECULE_PLUGINS_VERSION="23.5.3"

##
## ---------------------
## Available make target
## ---------------------
##

all: help
help: ## Display this message
	@grep -E '(^[a-zA-Z0-9_\-\.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

init: ## Setup the python venv
	mkdir -p venv
	python3 -m virtualenv -p /usr/bin/python${PYTHON_VERSION} venv

install: init ## Locally install ansible, ansible-lint, yamllint, mitogen plugin & molecule, using python3 venv
	( \
		. ${ANSIBLE_VENV}; \
		pip install \
			ansible=="${ANSIBLE_VERSION}" \
			ansible-lint=="${ANSIBLE_LINT_VERSION}" \
			yamllint=="${YAMLLINT_VERSION}" \
			molecule=="${MOLECULE_VERSION}" \
			molecule-plugins[docker]=="${MOLECULE_PLUGINS_VERSION}" \
			jmespath \
			"ara[server]"; \
		ansible --version; \
	)

qa: yaml.lint ansible.lint dockerfile.lint ## Run qa locally

yaml.lint: ## Run yaml linter
	( \
		. ${ANSIBLE_VENV}; \
		 yamllint .; \
	)

dockerfile.lint: ## Run hadolint
	find .gitlab/dockerfiles -name Dockerfile -print0 | xargs -0 -n1 hadolint

##
## -------
## Ansible
## -------
##
export ANSIBLE_CALLBACK_PLUGINS=venv/lib/python${PYTHON_VERSION}/site-packages/ara/plugins/callback

ansible.lint: ## Lint Ansible code
	( \
		. ${ANSIBLE_VENV}; \
		ansible-lint --force-color -c ansible/.ansible-lint ansible; \
	)

# ANSIBLE_ROLES=custom_fonts docker golang kubectl rust zsh
ANSIBLE_ROLES=custom_fonts
ansible.test: ## Test Ansible roles with molecule
	( \
		. ${ANSIBLE_VENV}; \
		for role in $(ANSIBLE_ROLES); do \
			echo 'Testing role "'$${role}'"...'; \
			cd ansible/roles/$${role}; \
			molecule --base-config ../../molecule/molecule.yml test --parallel --all --destroy always; \
			cd ../../..; \
		done \
	)

ansible.run: ## Run the Ansible playbook on the host machine
	( \
		. ${ANSIBLE_VENV}; \
		cd ansible; \
		ansible-playbook dotfiles.yml -v --ask-become-pass --diff --limit='env_local'; \
	)

##
## -------
## Testing
## -------
##

vagrant.start: ## Start vagrant test machine
	vagrant up --no-provision
	ssh-add .vagrant/machines/dotfiles.test/virtualbox/private_key

vagrant.provision: ## Run Ansible provision on vagrant host
	( \
		. ${ANSIBLE_VENV}; \
		cd ansible; \
		export ANSIBLE_HOST_KEY_CHECKING=False; \
		ansible-playbook dotfiles.yml -v --diff --limit='env_test'; \
	)

vagrant.destroy: ## Destroy vagrant test machine
	vagrant destroy
