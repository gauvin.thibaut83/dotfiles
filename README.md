# Dotfiles

This repository contain my personal configuration i use everyday at my home and at work.

It contains config files for:

- Clementine player
- dunst
- gtk-3.0
- i3 & i3status
- polybar
- rofi
- youtube-dl
- redshit
- Bash & Zsh aliases
- bash
- curl
- git & gitignore config
- Taskwarrior
- zsh & oh-my-zsh

The config is installed & keep up to date by deleting real files on your host & replace them by symlink to this repository

It also provides some Ansible roles for install & configure:

- system fonts (font-awesome & powerline)
- docker
- goland
- i3 & i3status
- kubectl
- All my packages (default, exa, playerctl, ripgrep & youtube-dl)

## How to install (for real)

#### Dependencies:

```bash
sudo apt-get update \
    && sudo apt-get install -y \
      make python3 python3-pip python3-setuptools python3-virtualenv
```

#### Steps:

- Clone project

  ```bash
  git clone git@gitlab.com:gauvin.thibaut83/dotfiles.git ~/dotfiles
  cd ~/dotfiles
  ```

- Setup Ansible using python venv

  ```bash
  make install
  ```

- Check `env_local.yml` values

  ```bash
  vim ansible/group_vars/env_local.yml
  ```

- Run Ansible provisioning on your hosts machine (localhost)

  ```bash
  make ansible.run
  # You will be prompted to enter your password
  ```

- Install config (create symlink)

  ```bash
  # Edit file, to manage what do you want
  vim setup.sh
  
  # Create symlinks
  ./setup.sh
  ```

Now you should reboot your computer, then at login screen, click on icons "⚙" and select i3 instead of gnome as windows manager

Your Done ! enjoy

---

## How to test (inside VM)

#### Dependencies:

- Virtualbox (>= v6.1.16)
- Vagrant (>= v2.2.13)

#### Steps:

- Additional dependencies

  ```bash
  sudo apt-get install nfs-common nfs-kernel-server libarchive-tools
  ```

- Init test vm

  ```bash
  make vagrant.start
  ```

- Run Ansible provisioning on your test VM

  ```bash
  make vagrant.provision
  ```

Now you should reboot your vm, run `vagrant reload` or use user interface  
Then at login screen, click on icons "⚙" and select i3 instead of gnome as windows manager

Your Done ! enjoy
