Vagrant.require_version ">= 1.9.0"

servers=[
    {
        :hostname   => "dotfiles.test",
        :primary    => true,
        :autostart  => true,
        :ip         => "192.168.56.42",
        :ram        => 4096,
        :cpu        => 2,
        :cpu_cap    => 100
    }
]

Vagrant.configure(2) do |config|
    servers.each_with_index do |(machine), index|
        config.vm.define machine[:hostname], primary: machine[:primary], autostart: machine[:autostart] do |node|
            # Base box
            node.vm.box = "ubuntu/jammy64"

            # Customize VM
            node.vm.hostname = machine[:hostname]

            # Port Forwarding
            node.vm.network "private_network", ip: machine[:ip]

            # Mount synced folder
            node.vm.synced_folder ".", "/vagrant", type: 'nfs', nfs_version: 4, nfs_udp: false

            node.vm.provider "virtualbox" do |vb|
                vb.gui = false

                vb.name = machine[:hostname]
                vb.memory = machine[:ram]
                vb.cpus = machine[:cpu]
                vb.customize ["modifyvm", :id, "--cpuexecutioncap", machine[:cpu_cap]]

                # Change the network card hardware for better performance
                vb.customize ["modifyvm", :id, "--nictype1", "virtio" ]
                vb.customize ["modifyvm", :id, "--nictype2", "virtio" ]

                # Suggested fix for slow network performance
                # see https://github.com/mitchellh/vagrant/issues/1807
                vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
                vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]

                # Enable symlink
                vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]

                # Set the timesync threshold to 10 seconds, instead of the default 20 minutes.
                vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]

                # Disable usb 2.0 support
                vb.customize ["modifyvm", :id, "--usb", "on"]
                vb.customize ["modifyvm", :id, "--usbehci", "off"]
            end
        end
    end
end
