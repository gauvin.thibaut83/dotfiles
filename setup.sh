#!/bin/bash

set -eu

create_link() {
    rm -rvf ~/.$1
    ln -s ~/dotfiles/$1 ~/.$1
    echo -e "Create symlink for \e[1;32m~/dotfiles/$1 -> ~/.$1\e[0m...\n"
}

create_link config/autorandr
create_link config/config/betterlockscreen
create_link config/Clementine/Clementine.conf
create_link config/dunst
create_link config/gtk-3.0
create_link config/i3
# create_link config/i3status # polybar fallback
create_link config/polybar
create_link config/rofi
create_link config/yt-dlp
# We need to copy file on location due to a bug with apparmor
# symlink not working, got "fopen: Permission denied" error
#create_link config/redshift.conf
create_link aliases
create_link bashrc
create_link completions
create_link curlrc
create_link gitconfig
create_link gitignore
create_link p10k.zsh
create_link terraformrc
create_link zshrc
