#!/usr/bin/env bash

set -u

start_polybar() {
  killall -q polybar
  while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

  screens=$(xrandr --query | grep " connected" | cut -d" " -f1)
  for screen in ${screens}; do
    MONITOR=${screen} polybar --quiet --reload example &
  done
}

start_polybar
