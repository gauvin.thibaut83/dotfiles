#!/bin/bash

holiday_date="2024-06-28"
current_date=$(date +%Y-%m-%d)

days_remaining=$(( ( $(date -d $holiday_date +%s) - $(date -d $current_date +%s) ) / 86400 ))

echo "$days_remaining days until holidays"
