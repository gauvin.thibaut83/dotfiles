#!/bin/bash

pkill dunst
dunst -config ~/dotfiles/config/dunst/dunstrc &

notify-send -u critical "Test message: critical test 1"
notify-send -u normal "Test message: normal test 2"
notify-send -u low "Test message: low test 3"

notify-send -u critical "Test message: critical test 4"
notify-send -u normal "Test message: normal test 5"
notify-send -u low "Test message: low test 6"

notify-send -u critical "Test message: critical test 7"
notify-send -u normal "Test message: normal test 8"
notify-send -u low "Test message: low test 9"

notify-send --icon=/usr/share/icons/gnome/48x48/actions/system-lock-screen.png -u critical "Test with Icon" "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
notify-send "Test notification with link" "Here is a link: https://github.com/dunst-project/dunst"
