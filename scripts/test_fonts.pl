# https://github.com/polybar/polybar/wiki/Fonts#debugging-font-issues
# Script to retrieve which font provide given icon
#
# May need to install Font::FreeType lib (with local::lib method)
#   $ perl -MCPAN -e 'install Font::FreeType'
#
# Usage
#   $ perl test-fonts.pl "😀"

use strict;
use warnings;
use Font::FreeType;

my ($char) = @ARGV;

foreach my $font_def (`fc-list`) {
    my ($file, $name) = split(/: /, $font_def);
    my $face = Font::FreeType->new->face($file);
    my $glyph = $face->glyph_from_char($char);
    if ($glyph) {
        print $font_def;
    }
}
