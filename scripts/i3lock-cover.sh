#!/bin/bash

# Simple script to generate an background image to be used by i3lock

set -eux

BACKGROUND="futur_city.png"
LOCK="1.png"

convert ~/dotfiles/assets/backgrounds/${BACKGROUND} \
  -font Courier-Bold \
  -pointsize 30 -fill White -gravity center \
  -annotate +0+250 "Type Password to Unlock" ~/dotfiles/assets/locks/${LOCK} \
  -gravity center -composite ~/dotfiles/assets/screenlock.png
